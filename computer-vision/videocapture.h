#ifndef VIDEOCAPTURE_H
#define VIDEOCAPTURE_H

#include <opencv2/opencv.hpp>

#include <QImage>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>


// ------------------
// class VideoCapture
// ------------------

class VideoCapture : public QThread
{
    Q_OBJECT

public:
    VideoCapture();
    ~VideoCapture();

    bool getIsOpened() const;
    void setIsOpened(bool newIsOpened);

    static bool CVMat2QImage(cv::Mat &im_m, QImage &im_q);
    static bool QImage2CVMat(QImage &im_q, cv::Mat &im_m);

public slots:
    bool open(const char* url);
    void close();

    void run() override;

private slots:
    bool capture(cv::Mat &im);

private:
    bool isOpened;
    unsigned long usecs;

    cv::VideoCapture *vcap;
    QMutex *mutex;

signals:
    void streamopened();
    void streamfailed();
    void streamclosed();

    void imcaptured(const cv::Mat &im);
};

#endif // VIDEOCAPTURE_H
