#ifndef COMPUTERVISION_H
#define COMPUTERVISION_H

#include <opencv2/opencv.hpp>
#include <algorithm>
#include <vector>
#include <string>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QFileInfo>


//---------------
// class CVObject
// --------------

class CVObject
{
public:
    const cv::Mat im;
    const std::string label;

    CVObject(cv::Mat &im, const char* label);
};

//--------------
// class License
// -------------

class License : public CVObject
{
public:
    const std::string license;

    License(cv::Mat &im, const char* license, const char* label="License Plate");
};

//---------------------
// class ComputerVision
// --------------------

class ComputerVision
{
public:
    bool MOdetection=true;
    bool LPdetection=true;

    /* MOVING OBJECT DETECTION PARAMETERS */
    int MOthreshold=127;
    int MOkernelsize=50;
    int MOiterations=3;
    double MOminArea=3e4;
    double MOmaxArea=1e8;
    double MOminAR=1.0/5.0;
    double MOmaxAR=5.0/1.0;

    /* LICENSE DETECTION PARAMETERS */
    int LPfDiameter=13;
    double LPfSigmaColor=15.0;
    double LPfSigmaSpace=15.0;
    double LPminHystThVal=30.0;
    double LPmaxHystThVal=200.0;
    double LPerrorContour=0.018;
    double LPminArea=3e3;
    double LPmaxArea=1e8;
    double LPminAR=2.0/1.0;
    double LPmaxAR=5.0/1.0;

    /* CHARACTER DETECTION PARAMETERS */
    int CDkernelsize=5;
    double CDminArea=3e2;
    double CDmaxArea=1e4;
    double CDminAR=1.0/3.0;
    double CDmaxAR=1.0;

    ComputerVision();
    ~ComputerVision();

    bool start(const char* filename);

    bool process(cv::Mat &im1, cv::Mat &im2, cv::Mat &res);

    bool loadJsonData(const char* filename, cv::Mat &trainingData, cv::Mat &labels);
    bool extractCharFeatures(cv::Mat &im, cv::Mat &features);

    cv::Mat getConfussionMatrix(const char* filename);

    std::vector<CVObject> detectMovingObjects(cv::Mat &im1, cv::Mat &im2, cv::Mat &res, cv::Scalar color=cv::Scalar(0, 0, 255));
    std::vector<License> detectLicenses(cv::Mat &im, cv::Mat &res, cv::Scalar color=cv::Scalar(0, 0, 255));

    const char* readLicense(cv::Mat &im, std::vector<cv::Point> &contour);
    char detectChar(cv::Mat &im);

private:
    cv::Ptr<cv::ml::SVM> svm;

};

#endif // COMPUTERVISION_H
