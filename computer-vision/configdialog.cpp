#include "configdialog.h"

ConfigDialog::ConfigDialog()
{
    contentsWidget = new QListWidget;
    contentsWidget->setViewMode(QListView::ListMode);
    contentsWidget->setMovement(QListView::Static);
    contentsWidget->setMaximumWidth(150);

    pagesWidget = new QStackedWidget;
    pagesWidget->addWidget(new MainConfigurationPage);
    pagesWidget->addWidget(new VideoConfigurationPage);
    pagesWidget->addWidget(new CVConfigurationPage);

    QPushButton *closeButton = new QPushButton(tr("Close"));

    createIcons();

    contentsWidget->setCurrentRow(0);

    connect(closeButton, &QAbstractButton::clicked, this, &QWidget::close);

    QHBoxLayout *horizontalLayout = new QHBoxLayout;
    horizontalLayout->addWidget(contentsWidget);
    horizontalLayout->addWidget(pagesWidget, 1);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(closeButton);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(horizontalLayout);
    mainLayout->addStretch(1);
    mainLayout->addSpacing(12);
    mainLayout->addLayout(buttonsLayout);
    setLayout(mainLayout);

    setWindowTitle(tr("Settings"));
}

void ConfigDialog::createIcons()
{
    QListWidgetItem *mainConfigButton = new QListWidgetItem(contentsWidget);
    mainConfigButton->setText(tr("Main"));
    mainConfigButton->setTextAlignment(Qt::AlignLeft);
    mainConfigButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QListWidgetItem *videoConfigButton = new QListWidgetItem(contentsWidget);
    videoConfigButton->setText(tr("Video"));
    videoConfigButton->setTextAlignment(Qt::AlignLeft);
    videoConfigButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QListWidgetItem *CVConfigButton = new QListWidgetItem(contentsWidget);
    CVConfigButton->setText(tr("Computer Vision"));
    CVConfigButton->setTextAlignment(Qt::AlignLeft);
    CVConfigButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    connect(contentsWidget, &QListWidget::currentItemChanged, this, &ConfigDialog::changePage);
}

void ConfigDialog::changePage(QListWidgetItem *current, QListWidgetItem *previous)
{
    if (!current)
        current = previous;

    pagesWidget->setCurrentIndex(contentsWidget->row(current));
}

MainConfigurationPage::MainConfigurationPage(QWidget *parent)
    : QWidget(parent)
{

}

VideoConfigurationPage::VideoConfigurationPage(QWidget *parent)
    : QWidget(parent)
{

}

CVConfigurationPage::CVConfigurationPage(QWidget *parent)
    : QWidget(parent)
{

}
