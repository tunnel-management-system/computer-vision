#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include <QtWidgets>


class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    ConfigDialog();

public slots:
    void changePage(QListWidgetItem *current, QListWidgetItem *previous);

private:
    void createIcons();

    QListWidget *contentsWidget;
    QStackedWidget *pagesWidget;
};

class MainConfigurationPage : public QWidget
{
public:
    MainConfigurationPage(QWidget *parent = 0);
};

class VideoConfigurationPage : public QWidget
{
public:
    VideoConfigurationPage(QWidget *parent = 0);
};

class CVConfigurationPage : public QWidget
{
public:
    CVConfigurationPage(QWidget *parent = 0);
};

#endif // CONFIGDIALOG_H
