#include "videocapture.h"

// ------------------
// class VideoCapture
// ------------------

VideoCapture::VideoCapture()
    : isOpened(false)
    , usecs(200000)
    , vcap(new cv::VideoCapture)
    , mutex(new QMutex)
{
    vcap->set(cv::CAP_PROP_BUFFERSIZE, 5);
}

VideoCapture::~VideoCapture()
{
    delete vcap;
    delete mutex;
}

bool VideoCapture::getIsOpened() const
{
    return isOpened;
}

void VideoCapture::setIsOpened(bool newIsOpened)
{
    isOpened = newIsOpened;
}

bool VideoCapture::open(const char* url)
{
    QMutexLocker locker(mutex);

    cv::String strURL = cv::String(url);

    if (strURL.empty()) {
        qWarning("Empty URL stream provided.");

        setIsOpened(false);
        emit streamfailed();

        return false;
    }

    if (!vcap->open(strURL, cv::CAP_FFMPEG)) {
        qWarning("Unable to open video stream.");

        setIsOpened(false);
        emit streamfailed();

        return false;
    }

    qInfo("Stream opened successfully.");

    setIsOpened(true);
    emit streamopened();

    return true;
}

void VideoCapture::close()
{
    QMutexLocker locker(mutex);

    vcap->release();

    qInfo("Stream closed.");

    setIsOpened(false);
    emit streamclosed();
}

bool VideoCapture::capture(cv::Mat &im)
{
    QMutexLocker locker(mutex);

    if (!vcap->isOpened()) {
        qWarning("Unable to open video stream.");
        return false;
    }

    *(vcap) >> im;

    if (im.empty()) {
        qWarning("Empty frame captured.");
        return false;
    }

    return true;
}

void VideoCapture::run()
{
    cv::Mat im;
    while(getIsOpened()) {
        capture(im);

        emit imcaptured(im);

        usleep(usecs);  // Sleep thread
    }
}

bool VideoCapture::CVMat2QImage(cv::Mat &im_m, QImage &im_q)
{
    switch (im_m.type()) {

    // 8-bit, 4 channel
    case CV_8UC4:
    {
        im_q = QImage(im_m.data,
                      im_m.cols,
                      im_m.rows,
                      static_cast<int>(im_m.step),
                      QImage::Format_ARGB32);
        return true;
    }

    // 8-bit, 3 channel
    case CV_8UC3:
    {
        im_q = QImage(im_m.data,
                      im_m.cols,
                      im_m.rows,
                      static_cast<int>(im_m.step),
                      QImage::Format_RGB888)
               .rgbSwapped();
        return true;
    }

    // 8-bit, 1 channel
    case CV_8UC1:
    {
        im_q = QImage(im_m.data,
                      im_m.cols,
                      im_m.rows,
                      static_cast<int>(im_m.step),
                      QImage::Format_Grayscale8);
        return true;
    }

    default:
        qWarning("Cannot convert cv::Mat to type QImage.");
        return false;

    }
}

bool VideoCapture::QImage2CVMat(QImage &im_q, cv::Mat &im_m)
{
    switch(im_q.format()) {

    // 8-bit, 4 channel
    case QImage::Format_ARGB32:
    {
        im_m = cv::Mat(im_q.height(),
                       im_q.width(),
                       CV_8UC4,
                       (cv::Scalar*)im_q.scanLine(0));
        return true;
    }

    // 8-bit, 3 channel
    case QImage::Format_RGB888:
    {
        im_m = cv::Mat(im_q.height(),
                       im_q.width(),
                       CV_8UC3,
                       (cv::Scalar*)im_q.scanLine(0));
        cv::cvtColor(im_m, im_m, cv::COLOR_BGR2RGB);
        return true;
    }

    // 8-bit, 1 channel
    case QImage::Format_Grayscale8:
    {
        im_m = cv::Mat(im_q.height(),
                       im_q.width(),
                       CV_8UC1,
                       (cv::Scalar*)im_q.scanLine(0));
        return true;
    }

    default:
        qWarning("Cannot convert QImage to type cv::Mat.");
        return false;

    }
}
