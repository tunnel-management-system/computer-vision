#include "mainwindow.h"
#include "computervision.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    if (argc > 1) {
        QCoreApplication app(argc, argv);
        QCoreApplication::setApplicationName("computer-vision");
        QCoreApplication::setApplicationVersion("1.0.0");

        QCommandLineParser parser;
        parser.setApplicationDescription("Computer Vision project for object detection and license plate recognition.");
        parser.addHelpOption();
        parser.addVersionOption();

        parser.addPositionalArgument("image", QCoreApplication::translate("main", "Image to process."));
        parser.addPositionalArgument("image2", QCoreApplication::translate("main", "Image 2 to process."));
        QCommandLineOption jsonFileOption(QStringList() << "j" << "json",
                    QCoreApplication::translate("main", "Load training data from file <json-file>"),
                    QCoreApplication::translate("main", "json-file"));
            parser.addOption(jsonFileOption);

        // Process the actual command line arguments given by the user
        parser.process(app);

        const QStringList args = parser.positionalArguments();

        QString imFile1, imFile2;
        imFile1 = args.at(0);
        if (args.size() > 1) imFile2 = args.at(1);
        else imFile2 = imFile1;

        QByteArray ba1 = imFile1.toUtf8();
        QByteArray ba2 = imFile2.toUtf8();

        QString jsonFile = parser.value(jsonFileOption);
        QByteArray ba = jsonFile.toUtf8();

        ComputerVision cv = ComputerVision();

        cv.start(ba.data());

        cv::Mat im1, im2, res;
        im1 = cv::imread(ba1.data());
        im2 = cv::imread(ba2.data());

        cv.process(im1, im2, res);

        cv::namedWindow("Processed", cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);
        cv::imshow("Processed", res);
        cv::waitKey(0);

        return 0;
    }

    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowState(Qt::WindowMaximized);  // Maximize window
    w.show();
    return a.exec();
}
