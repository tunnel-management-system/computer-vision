#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QtWidgets>
#include <QSettings>
#include <QImage>
#include <QPixmap>
#include <QDir>

#include "videocapture.h"
#include "computervision.h"
#include "configdialog.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    const cv::Mat &getIm() const;
    void setIm(const cv::Mat &newIm);

private:
    QSettings *s;

    cv::Mat im;
    VideoCapture *vcap;
    QLabel *videoWidget;

    ComputerVision *cv;

    void buildUi();
    void createMenus();

private slots:
    void startcapture();
    void stopcapture();
    void errorcapture();

    void process(const cv::Mat &im);
    void display(const cv::Mat &im);

    void openstreamdialog();

    void openstream(const QString url);
    void closestream();
    void settings();
    void onlinehelp();
    void about();

    void saveframe();

};
#endif // MAINWINDOW_H
