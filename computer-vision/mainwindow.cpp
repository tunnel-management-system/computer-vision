#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , s(new QSettings)
    , vcap(new VideoCapture)
    , videoWidget(0)
    , cv(new ComputerVision)
{
    connect(vcap, &VideoCapture::streamopened, this, &MainWindow::startcapture);
    connect(vcap, &VideoCapture::streamclosed, this, &MainWindow::stopcapture);
    connect(vcap, &VideoCapture::streamfailed, this, &MainWindow::errorcapture);
    connect(vcap, &VideoCapture::imcaptured, this, &MainWindow::process);

    connect(this, &MainWindow::destroyed, vcap, &VideoCapture::close);

    buildUi();

    // Start ComputerVision object
    QString strFilename = s->value("cv/trainingfile", "data/c.json").toString();
    QByteArray ba = strFilename.toUtf8();
    cv->start(ba.data());
}

MainWindow::~MainWindow()
{
    delete s;
    delete vcap;
    delete videoWidget;
    delete cv;
}

const cv::Mat &MainWindow::getIm() const
{
    return im;
}

void MainWindow::setIm(const cv::Mat &newIm)
{
    im = newIm;
}

void MainWindow::buildUi()
{
    // Create main content widget
    QWidget *mainWidget = new QWidget;
    mainWidget->setStyleSheet("background-color:black");
    setCentralWidget(mainWidget);

    videoWidget = new QLabel;  // Label widget to display video frames

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(videoWidget);

    mainWidget->setLayout(layout);  // Set layout to main widget

    createMenus();

    QString message = tr("Welcome");
    statusBar()->showMessage(message);

    setWindowTitle(tr("Computer Vision"));
    setMinimumSize(s->value("mainwindow/minwidth", 300).toInt(), s->value("mainwindow/minheight", 300).toInt());
}

void MainWindow::createMenus()
{
    QAction *openAct = new QAction(tr("&Open"), this);
    openAct->setStatusTip(tr("Open new stream"));
    openAct->setShortcut(QKeySequence(tr("Ctrl+O")));
    connect(openAct, &QAction::triggered, this, &MainWindow::openstreamdialog);

    QAction *closeAct = new QAction(tr("&Close"), this);
    closeAct->setStatusTip(tr("Close stream"));
    closeAct->setShortcut(QKeySequence(tr("Ctrl+W")));
    connect(closeAct, &QAction::triggered, this, &MainWindow::closestream);

    QAction *captureAct = new QAction(tr("&Capture"), this);
    captureAct->setStatusTip(tr("Capture frame"));
    captureAct->setShortcut(QKeySequence(tr("Ctrl+S")));
    connect(captureAct, &QAction::triggered, this, &MainWindow::saveframe);

    QAction *settingsAct = new QAction(tr("&Settings"), this);
    settingsAct->setStatusTip(tr("Settings"));
    settingsAct->setShortcut(QKeySequence(tr("Ctrl+T")));
    connect(settingsAct, &QAction::triggered, this, &MainWindow::settings);

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openAct);
    fileMenu->addAction(closeAct);
    fileMenu->addAction(captureAct);
    fileMenu->addAction(settingsAct);


    QAction *helpAct = new QAction(tr("&Online help"), this);
    helpAct->setStatusTip(tr("Open online help"));
    helpAct->setShortcut(QKeySequence(tr("Ctrl+H")));
    connect(helpAct, &QAction::triggered, this, &MainWindow::onlinehelp);

    QAction *aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("About this project"));
    connect(aboutAct, &QAction::triggered, this, &MainWindow::about);

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(helpAct);
    helpMenu->addAction(aboutAct);
}

void MainWindow::startcapture()
{
    stopcapture();

    vcap->start();  // Start capture thread

    statusBar()->showMessage(tr("Started"));
}

void MainWindow::stopcapture()
{
    videoWidget->clear();
    statusBar()->showMessage(tr("Stopped"));
}

void MainWindow::errorcapture()
{
    videoWidget->clear();
    statusBar()->showMessage(tr("Failed"));
}

void MainWindow::process(const cv::Mat &im)
{
    cv::Mat im1 = im;
    cv::Mat im2 = getIm();

    setIm(im.clone());  // Save previous frame

    if (im1.empty() || im2.empty() || im1.size != im2.size) {
        qWarning("Frames empty or different sizes");
        return;
    }

    cv::Mat im_proc;
    if (!cv->process(im1, im2, im_proc)) {
        qWarning("Processing error");
        return;
    }

    display(im_proc);
}


void MainWindow::display(const cv::Mat &im)
{
    QImage im_q;
    cv::Mat im_m = cv::Mat(im);
    if (!VideoCapture::CVMat2QImage(im_m, im_q)) {
        return;
    }

    QPixmap pix = QPixmap::fromImage(im_q);
    videoWidget->setPixmap(pix.scaled(videoWidget->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    videoWidget->setAlignment(Qt::AlignCenter);
}

void MainWindow::openstreamdialog()
{
    bool ok;
    QString url = QInputDialog::getText(this,
                                        tr("Stream URL"),
                                        tr("URL:"), QLineEdit::Normal,
                                        s->value("video/url", "rtsp://admin:@192.168.1.101:8554/profile0").toString(), // "C:\\Users\\apabolleta\\Videos\\traffic.mp4" "rtsp://admin:@192.168.1.101:8554/profile0"
                                        &ok);

    if (ok && !url.isEmpty()) {
        s->setValue("video/url", url);  // Save URL settings
        openstream(url);
    }
}

void MainWindow::openstream(const QString url)
{
    if (url.isEmpty())
        return;

    QString msg = "Opening stream: ";
    msg += url;
    statusBar()->showMessage(msg);

    closestream();

    QByteArray ba = url.toUtf8();
    if (vcap->open(ba.data())) {
        statusBar()->showMessage("Stream opened");
    }

}

void MainWindow::closestream()
{
    vcap->close();
    statusBar()->showMessage("Stream closed");
}

void MainWindow::settings()
{
    ConfigDialog dialog;
    dialog.exec();
}

void MainWindow::onlinehelp()
{
    QDesktopServices::openUrl(QUrl("https://gitlab.com/tunnel-management-system/computer-vision/-/wikis/home"));
}

void MainWindow::about()
{
    QMessageBox::about(this,
                       tr("About the Application"),
                       tr("This application is developed for computer vision testing purposes."));
}

void MainWindow::saveframe()
{
    cv::Mat img = im.clone();

    if (img.empty())
        return;

    QString strFilename = QFileDialog::getSaveFileName(this,
                                                       tr("Save frame"),
                                                       QDir::homePath() + "/capture.png",
                                                       tr("Images (*.png *.jpg)"));

    if (strFilename.isEmpty())
        return;

    QByteArray ba = strFilename.toUtf8();
    cv::imwrite(ba.data(), img);
}
