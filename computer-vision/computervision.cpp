#include "computervision.h"

//---------------
// class CVObject
// --------------

CVObject::CVObject(cv::Mat &im, const char* label)
    : im(im)
    , label(std::string(label))
{

}

//--------------
// class License
// -------------

License::License(cv::Mat &im, const char* license, const char* label)
    : CVObject(im, label)
    , license(std::string(license))
{

}

//---------------------
// class ComputerVision
// --------------------

ComputerVision::ComputerVision()
    : svm(cv::ml::SVM::create())
{
    // Set the SVM
    svm->setType(cv::ml::SVM::C_SVC);
    svm->setKernel(cv::ml::SVM::LINEAR);
    svm->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, 100, 1e-6));
}

ComputerVision::~ComputerVision()
{

}

bool ComputerVision::start(const char* filename)
{
    // ----------------------
    // LOAD SVM TRAINING DATA
    // ----------------------

    cv::Mat trainingData, labels;
    loadJsonData(filename, trainingData, labels);

    if (trainingData.empty() || labels.empty()) {
        return false;
    }

    // ---------
    // TRAIN SVM
    // ---------

    svm->train(trainingData, cv::ml::ROW_SAMPLE, labels);

    return true;
}

bool ComputerVision::process(cv::Mat &im1, cv::Mat &im2, cv::Mat &res)
{
    res = im1.clone();

    // DETECT MOVING OBJECTS
    if(MOdetection) {
        std::vector<CVObject> MObjects;
        MObjects = detectMovingObjects(im1, im2, res, cv::Scalar(0, 0, 255));
    }

    // DETECT LICENSE PLATES
    if (LPdetection) {
        std::vector<License> licenses;
        licenses = detectLicenses(im1, res, cv::Scalar(0, 255, 0));
    }

    return true;
}

std::vector<CVObject> ComputerVision::detectMovingObjects(cv::Mat &im1, cv::Mat &im2, cv::Mat &res, cv::Scalar color)
{
    std::vector<CVObject> objects;
    cv::Mat img1, img2, img;

    // GRAYSCALE IMAGES
    cv::cvtColor(im1, img1, cv::COLOR_BGR2GRAY);
    cv::cvtColor(im2, img2, cv::COLOR_BGR2GRAY);

    // FRAME DIFFERENCING
    cv::absdiff(img1, img2, img);

    // IMAGE THRESHOLDING ---> Binarization using fixed threshold value
    cv::threshold(img, img, MOthreshold, 255, cv::THRESH_BINARY);

    // MORPHOLOGICAL TRANSFORMATION ---> dilation
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(MOkernelsize, MOkernelsize));
    cv::morphologyEx(img, img, cv::MORPH_DILATE, kernel, cv::Point(-1, -1), MOiterations);

    // FIND CONTOURS
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(img, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    // SORT CONTOURS ---> sort by contour area (largest to smallest)
    auto fSortContours = [](std::vector<cv::Point> c1, std::vector<cv::Point> c2) {
        return (cv::contourArea(c1) > cv::contourArea(c2));
    };
    sort(contours.begin(), contours.end(), fSortContours);

    // EVALUATE CONTOURS
    for (size_t i = 0; i < contours.size(); i++) {

        std::vector<cv::Point> currentContour = contours[(int)i];

        cv::Rect r = cv::boundingRect(currentContour);

        double area = r.area();
        double aspectRatio = r.height == 0 ? 0 : (double)r.width / (double)r.height;

        if (area < MOminArea) break;
        else if (!(MOminArea <= area && area <= MOmaxArea)) continue;
        else if (!(MOminAR <= aspectRatio && aspectRatio <= MOmaxAR)) continue;

        // Draw object bounding rectangle
        cv::rectangle(res, r, color, 1, cv::LINE_8, 0);

        const char* label = "Moving Object";

        // Write object label
        cv::Point rcenter = (r.br() + r.tl())*0.5;  // Get central point
        cv::putText(res, label, rcenter, cv::FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1, cv::LINE_8);

        // Save object to vector
        cv::Mat oIm = im1(r);
        CVObject o = CVObject(oIm, label);
        objects.push_back(o);

    }

    return objects;
}

std::vector<License> ComputerVision::detectLicenses(cv::Mat &im, cv::Mat &res, cv::Scalar color)
{
    std::vector<License> licenses;
    cv::Mat img, imf;

    // GRAYSCALE IMAGE
    cv::cvtColor(im, img, cv::COLOR_BGR2GRAY);

    // FILTER IMAGE ---> bilateral filter
    cv::bilateralFilter(img, imf, LPfDiameter, LPfSigmaColor, LPfSigmaSpace);

    // EDGE DETECTION ---> Canny
    cv::Canny(imf, img, LPminHystThVal, LPmaxHystThVal);

    // FIND CONTOURS
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(img, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    // SORT CONTOURS ---> sort by contour area (largest to smallest)
    auto fSortContours = [](std::vector<cv::Point> c1, std::vector<cv::Point> c2) {
        return (cv::contourArea(c1) > cv::contourArea(c2));
    };
    sort(contours.begin(), contours.end(), fSortContours);

    // EVALUATE CONTOURS
    for (size_t i = 0; i < contours.size(); i++) {

        std::vector<cv::Point> currentContour, approxContour;
        currentContour = contours[(int)i];

        double approxEpsilon = LPerrorContour * cv::arcLength(currentContour, true);
        cv::approxPolyDP(currentContour, approxContour, approxEpsilon, true);

        cv::Rect r = cv::boundingRect(approxContour);

        bool isConvex = cv::isContourConvex(approxContour);
        size_t size = approxContour.size();
        double area = r.area();
        double aspectRatio = r.height == 0 ? 0 : (double)r.width / (double)r.height;

        if (area < LPminArea) break;
        else if (!isConvex) continue;
        else if (size != 4) continue;
        else if (!(LPminArea <= area && area <= LPmaxArea)) continue;
        else if (!(LPminAR <= aspectRatio && aspectRatio <= LPmaxAR)) continue;

        // Draw license bounding rectangle
        cv::rectangle(res, r, color, 1, cv::LINE_8, 0);

        const char* label = "License Plate";

        // Write object label
        cv::Point rcenter = (r.br() + r.tl())*0.5;  // Get central point
        cv::putText(res, label, rcenter, cv::FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1, cv::LINE_8);

        // Write license number
        const char* license = readLicense(im, approxContour);  // Read license
        cv::putText(res, license, r.tl(), cv::FONT_HERSHEY_COMPLEX_SMALL, 1, color, 1, cv::LINE_8);

        // Save license to vector
        cv::Mat lIm = im(r);
        License l = License(lIm, license);
        licenses.push_back(l);

    }

    return licenses;
}

const char* ComputerVision::readLicense(cv::Mat &im, std::vector<cv::Point> &contour)
{
    std::string *license = new std::string("");

    // ----------------------------------------
    // LICENSE PLATE WARP PERSPECTIVE TRANSFORM
    // ----------------------------------------

    cv::Mat im_dst;

    cv::Rect r = cv::boundingRect(contour);
    cv::RotatedRect rr = cv::minAreaRect(contour);

    // Get transformation points
    cv::Point2f in_pts[4], out_pts[4];

    rr.points(in_pts);  // Rectangle vertices. Order: bottomLeft, topLeft, topRight, bottomRight

    out_pts[0] = r.tl() + cv::Point(0, r.height);  // bottomLeft
    out_pts[1] = r.tl();                           // topLeft
    out_pts[2] = r.br() - cv::Point(0, r.height);  // topRight
    out_pts[3] = r.br();                           // bottomRight

    // Sort points
    auto fSortPoints = [](cv::Point2f p1, cv::Point2f p2) {
        return p1.x < p2.x;
    };
    std::sort(std::begin(in_pts), std::end(in_pts), fSortPoints);

    // Rotation correction
    if (in_pts[0].y < in_pts[1].y) {
        swap(in_pts[0], in_pts[1]);
    }
    if (in_pts[2].y > in_pts[3].y) {
        swap(in_pts[2], in_pts[3]);
    }

    cv::Mat M = cv::getPerspectiveTransform(in_pts, out_pts);
    cv::warpPerspective(im(r), im_dst, M, im.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);

    // ------------------
    // SEGMENT CHARACTERS
    // ------------------

    cv::Mat img;

    // GRAYSCALE IMAGE
    cv::cvtColor(im_dst, img, cv::COLOR_BGR2GRAY);

    // BINARIZATION ---> Otsu's thresholding
    cv::threshold(img, img, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

    // MORPHOLOGICAL TRANSFORMATION ---> opening (remove noise)
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(CDkernelsize, CDkernelsize));
    cv::morphologyEx(img, img, cv::MORPH_OPEN, kernel);

    // FIND CONTOURS
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(img, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    // SORT CONTOURS ---> sort contours left-to-right
    auto fSortContours = [](std::vector<cv::Point> c1, std::vector<cv::Point> c2) {
        cv::Rect r1 = cv::boundingRect(c1);
        cv::Rect r2 = cv::boundingRect(c2);

        return (r1.x < r2.x);
    };
    sort(contours.begin(), contours.end(), fSortContours);

    // EVALUATE CONTOURS
    for (size_t i = 0; i < contours.size(); i++) {

        std::vector<cv::Point> charContour = contours[(int)i];

        cv::Rect r = cv::boundingRect(charContour);

        double area = r.area();
        double aspectRatio = r.height == 0 ? 0 : (double)r.width / (double)r.height;

        if (!(CDminArea <= area && area <= CDmaxArea)) continue;
        else if (!(CDminAR <= aspectRatio && aspectRatio <= CDmaxAR)) continue;

        // ------------------------
        // CHARACTER CLASSIFICATION
        // ------------------------

        cv::Mat cIm = im_dst(r);  // Character image
        char c = detectChar(cIm); // Character detection

        license->push_back(c);

    }

    return license->c_str();
}

char ComputerVision::detectChar(cv::Mat &im)
{
    cv::Mat features;

    // Extract char features
    extractCharFeatures(im, features);

    // Predict
    float cF = svm->predict(features);
    char c = (char)(uint)cF;

    return c;
}

bool ComputerVision::loadJsonData(const char* filename, cv::Mat &trainingData, cv::Mat &labels)
{
    QString strFilename(filename);
    QFile f(strFilename);

    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    QFileInfo fInfo(f);
    QString strFilePath = fInfo.path();

    QByteArray trainData = f.readAll();

    // JSON object from file contents
    QJsonDocument jsonDocument(QJsonDocument::fromJson(trainData));
    QJsonObject json = jsonDocument.object();

    if (!(json.contains("data") && json["data"].isArray())) {
        return false;
    }

    QJsonArray dataArray = json["data"].toArray();

    for(int i = 0; i < dataArray.size(); i++) {
        QJsonObject dataObject = dataArray[i].toObject();
        QJsonObject attrObject = dataObject["attr"].toObject();

        QString strImFilename = dataObject["file"].toString();
        QString strImFilePath = strFilePath + "/" + strImFilename;

        QByteArray ba = strImFilePath.toUtf8();
        cv::String imFilePath = cv::String(ba.data());

        // Read image and get label
        cv::Mat im = cv::imread(imFilePath);
        if (im.empty()) {
            return false;
        }
        float label = (float)attrObject["label"].toString().front().unicode();

        // Extract features
        cv::Mat cFeatures;
        extractCharFeatures(im, cFeatures);

        // Store to training data matrix
        trainingData.push_back(cFeatures);
        labels.push_back(label);
    }

    // Convert to appropiate data type
    trainingData.convertTo(trainingData, CV_32F);
    labels.convertTo(labels, CV_32SC1);

    return true;
}

bool ComputerVision::extractCharFeatures(cv::Mat &im, cv::Mat &features)
{
    cv::Mat img;
    std::vector<float> featureVector;

    cv::Size imSize = cv::Size(60, 200);  // Normalize images to fixed size

    // SCALE IMAGE
    cv::resize(im, img, imSize);

    // GRAYSCALE IMAGE
    cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);

    // BINARIZATION ---> Binary thresholding
    cv::threshold(img, img, 127, 255, cv::THRESH_BINARY);

    // FEATURE EXTRACTION TOOLS
    // Lambda function to get black pixel percentage for a given image
    auto fPercBlack = [](cv::Mat &im) {
        int nPixels = im.rows * im.cols;
        int nBlack = nPixels - cv::countNonZero(im);

        return (float) nBlack / nPixels;
    };

    // Lambda function to subdivide image
    auto fSubdivideMat = [](cv::Mat &im, int rowNDiv, int colNDiv) {
        std::vector<cv::Mat> subM;
        cv::Rect r;

        if (im.empty() || im.cols % colNDiv != 0 || im.rows % rowNDiv != 0)
            return subM;

        for (int y = 0; y < im.cols; y += im.cols / colNDiv) {
            for (int x = 0; x < im.rows; x += im.rows / rowNDiv) {
                r = cv::Rect(y, x, (im.cols / colNDiv), (im.rows / rowNDiv));
                subM.push_back(im(r).clone());
            }
        }

        return subM;
    };

    // ------------------
    // FEATURE EXTRACTION
    // ------------------

    // Total black pixel percentage
    float totalPercBlack = fPercBlack(img);
    featureVector.push_back(totalPercBlack);

    // Black pixel percentage per area 4x3
    std::vector<cv::Mat> subIm = fSubdivideMat(img, 4, 3);
    for (size_t i = 0; i < subIm.size(); i++) {
        float percBlack = fPercBlack(subIm[(int)i]);
        featureVector.push_back(percBlack);
    }

    // Number of contours
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(img, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    featureVector.push_back((float)contours.size());

    features = cv::Mat(1, (int)featureVector.size(), CV_32F, featureVector.data());

    return true;
}

cv::Mat ComputerVision::getConfussionMatrix(const char* filename)
{
    cv::Mat data, labels;
    loadJsonData(filename, data, labels);

    cv::Mat confussionMatrix = cv::Mat::zeros(10, 10, CV_32SC1);

    for (int i = 0; i < data.rows; i++) {
        int cInt = (int)svm->predict(data.row(i)) - (int)'0';
        int c = labels.at<int>(i) - (int)'0';

        confussionMatrix.at<int>(c, cInt) += 1;

    }

    return confussionMatrix;
}
