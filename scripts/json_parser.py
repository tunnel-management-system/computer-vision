"""
This Python script renames all files in the current directory and generates corresponding JSON file for SVM classifier training process.

"""

import os
import json

template = lambda i, fn : {
    "id": f"{i:03}",
    "file": fn,
    "attr": {
        "label": ""
    }
}

f_json = {
    "version": 1.0,
    "date": "2022-01-01",
    "author": "",
    "data": []
}

# Get files from current directory tree
im_files = [ os.path.join(dp, f) for dp, dn, filenames in os.walk(os.getcwd()) for f in filenames ]

data = [] # List to store data entries from template
i = 1     # Entry count
for f in im_files:

    f_name, f_extension = os.path.splitext(f)

    if (not f_extension in [".py", ".json"]):

        fn = f"{i:03}{f_extension}" # New file name

        # Rename file with numeric id and corresponding file extension
        os.rename(f, fn)

        # Generate JSON entry
        d = template(i, fn)

        # Append to list
        data.append(d)

        i += 1 # Increment count

f_json['data'] = data

# Serializing json
o_json = json.dumps(f_json, indent=4)

# Write to c.json
with open("c.json", "w") as out_file:
    out_file.write(o_json)
