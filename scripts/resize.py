"""
Python script to resize images to a fized size in the current working directory.

"""

import os
import cv2

IM_SIZE = (60, 200)  # Sizing size

# Get files from current directory tree
im_files = [ os.path.join(dp, f) for dp, dn, filenames in os.walk(os.getcwd()) for f in filenames ]

for f in im_files:

    f_name, f_extension = os.path.splitext(f)

    if (not f_extension in [".py", ".json"]):
        # Read image
        img = cv2.imread(f)

        # Scale image to fixed size
        img = cv2.resize(img, IM_SIZE)

        # Save
        cv2.imwrite(f, img)
