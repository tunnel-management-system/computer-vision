"""
Python script to perform binary inversion operation on images in the current working directory and save as PNG.

"""

import os
import cv2

# Get files from current directory tree
im_files = [ os.path.join(dp, f) for dp, dn, filenames in os.walk(os.getcwd()) for f in filenames ]

for f in im_files:

    f_name, f_extension = os.path.splitext(f)

    if (not f_extension in [".py", ".json"]):
        # Read image
        img = cv2.imread(f)

        # Binarize image and invert bitwise
        ret, img = cv2.threshold(img, 50, 255, cv2.THRESH_BINARY)
        img = cv2.bitwise_not(img)

        # Save as PNG
        cv2.imwrite(f_name + ".png", img)
